import https = require("https")
import http = require("http")
const config = require("config")

import {
    ChatMessageEvent,
    UserJoinedEvent,
    NameChangeEvent,
    StreamStartedEvent,
    StreamStoppedEvent,
    VisibilityUpdatedEvent,
    ChatEvent,
    EventType
} from "../../interfaces"

export function parseOwncastChatEvent(owncastMessage): ChatEvent | ChatMessageEvent {
    switch (owncastMessage.type) {
        case "CHAT":
            return {
                type: EventType.chatMessage,
                author: {
                    id: owncastMessage.eventData.user.id,
                    displayName: owncastMessage.eventData.user.displayName,
                },
                body: owncastMessage.eventData.body
            } as ChatMessageEvent
        break;
        case 'USER_JOINED':
            return {
                type: EventType.userJoined,
                userId: owncastMessage.eventData.user.id,
                userDisplayName: owncastMessage.eventData.user.displayName,
                isAuthenticated: owncastMessage.eventData.user.authenticated
            } as UserJoinedEvent
        break;
        case 'NAME_CHANGE':
            return {
                type: EventType.nameChanged,
                userId: owncastMessage.eventData.user.id,
                userDisplayName: owncastMessage.eventData.user.displayName,
                previousDisplayName: owncastMessage.eventData.user.previousNames.at(-1),
                isAuthenticated: owncastMessage.eventData.user.authenticated,
                previousNames: owncastMessage.eventData.user.previousNames
            } as NameChangeEvent
        break;
        case "STREAM_STARTED":
            return {
                type: EventType.streamStarted,
                streamTitle: owncastMessage.eventData.streamTitle,
                timestamp: owncastMessage.eventData.timestamp
            } as StreamStartedEvent
            break;
        case "STREAM_STOPPED":
            return {
                type: EventType.streamStopped,
                timestamp: owncastMessage.eventData.timestamp
            } as StreamStoppedEvent
            break;
        case "VISIBILITY-UPDATE":
            return {
                type: EventType.visibilityUpdated,
                visible: owncastMessage.eventData.visible,
                ids: owncastMessage.eventData.ids
            } as VisibilityUpdatedEvent
            break;
    }
}

export function postOwncastChatMessage(message: string, token: string) {
    console.log(`Sending chat message to owncast: "${message}"`)
    const options: https.RequestOptions = {
        host: config.get("owncast.host"),
        path: "/api/integrations/chat/send",
        port: config.get("owncast.port"),
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + config.get("owncast.chat_send_token"),
        }
    }
    if (config.get("owncast.force_ipv4") === true) {
        options.family = 4
    }

    function onResponse(response) {
        var data = ""
        response.on("data", function (chunk) {
            data += chunk; //Append each chunk of data received to this variable.
        })
        response.on("end", function () {
            console.log("server response: " + data); //Display the server's response, if any.
        })
    }

    let request = https.request(options, onResponse)
    if (config.get("owncast.scheme") == "http") {
        request = http.request(options, onResponse)
    }
    request.write(JSON.stringify({ body: message }))
    request.end()
}


