import {
    ChatbotCustomTrigger,
} from "../../interfaces"

export async function dynamicText(command: ChatbotCustomTrigger, token: string) {
    const commandCode = await import(`../../commands/${command.family}/${command.sourcefile}.js`)
    const tag = `${command.id}-${token}`
    // Call the default method for the command, passing the chat message's text as parameter
    const text: string = await commandCode.default(command, tag)
    return text
}

export async function overlay(command: ChatbotCustomTrigger, token: string) {
    const commandCode = await import(`../../commands/${command.family}/${command.sourcefile}.js`)
    const tag = `${command.id}-${token}`
    // Call the default method for the command, passing the chat message's text as parameter
    commandCode.default(command, tag)
}

export async function alertOverlay(command: ChatbotCustomTrigger, token: string) {
    // Replace placeholders in labels
    command = replacePlaceholders(command);
    // Send the alert to the client
    global.io.to(`alert-overlay-${token}`).emit('new_alert', JSON.stringify(command.parameters))
}

function replacePlaceholders( command: ChatbotCustomTrigger): ChatbotCustomTrigger {
    const modifiedChatbotCustomTrigger = JSON.parse(JSON.stringify(command));
    // Replace placeholders in labels
    if ( modifiedChatbotCustomTrigger.parameters["labels"] && Array.isArray( modifiedChatbotCustomTrigger.parameters["labels"])) {
        for (let i=0; i< modifiedChatbotCustomTrigger.parameters["labels"].length; i++) {
            console.log("label:",  modifiedChatbotCustomTrigger.parameters["labels"][i]["text"]);
             modifiedChatbotCustomTrigger.parameters["labels"][i]["text"] = ( modifiedChatbotCustomTrigger.parameters["labels"][i]["text"] as string).replace('{author}',  modifiedChatbotCustomTrigger.message.author.displayName)
             modifiedChatbotCustomTrigger.parameters["labels"][i]["text"] = ( modifiedChatbotCustomTrigger.parameters["labels"][i]["text"] as string).replace('{message}',  modifiedChatbotCustomTrigger.message.body)
        }
    }

    return modifiedChatbotCustomTrigger;
    // TODO we should allow placeholders in simple-text too    
}