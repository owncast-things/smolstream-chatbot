import glob = require("glob");
import fs = require("fs");
const config = require("config")
import {
    ChatbotCustomTrigger,
    ChatbotOverlay,
    ImportedJsonData,
    JSONContent,
    StaticRoute,
    TriggerType
} from "../../interfaces"

function registerFamilyStaticRoute(commandFamilyId: string, app, express): StaticRoute {
    const staticFolderPath = `./commands/${commandFamilyId}/static`
    if (fs.existsSync(staticFolderPath)) {
        const commandStaticRoute = `/static/${commandFamilyId}`
        console.log(`  - Registering route "${commandStaticRoute}"`)
        //console.log(`    for folder "${staticFolderPath}"`)
        app.use(commandStaticRoute, express.static(staticFolderPath))
        return {
            "family": commandFamilyId,
            "route": commandStaticRoute
        }
    }
    return null
}

function fixStaticFilesURLsForCommand(command: ChatbotCustomTrigger, staticRoute: string) {
    if (command.parameters) {
        const attributesToFix: Array<string> = ['image', 'video', 'sound']
        attributesToFix.forEach((attribute) => {
            // We need to check if the attribute has already been fixed, 
            // as this function can be called multiple times, if there
            // are multiple triggers for the same command
            if (attribute in command.parameters && !command.parameters[attribute].startsWith(staticRoute)) {
                command.parameters[attribute] = `${config.get('chatbot.prefix')}${staticRoute}/${command.parameters[attribute]}`
            }
        })
    }
}

function sanitizeSourceFileName(sourcefile: string) {
    // Remove all slashes, dots, and spaces
    sourcefile = sourcefile.replace(/[\/\\\.\s]/g, "")
    // remove non ascii characters
    sourcefile = sourcefile.replace(/[^\x00-\x7F]/g, "")
    return sourcefile
}

export function loadJsonFiles(app, express): ImportedJsonData {
    let triggersArray: Array<ChatbotCustomTrigger> = []
    let triggerlessArray: Array<ChatbotCustomTrigger> = []
    let eventTriggersArray: Array<ChatbotCustomTrigger> = []
    let overlaysArray: Array<ChatbotOverlay> = []
    for (let fileName of glob.sync("commands/*.json")) {
        const fileContent: JSONContent = JSON.parse(fs.readFileSync(fileName, 'utf8'))
        console.log(`Loading triggers for ${fileContent.label}`)
        const registeredStaticRoute = registerFamilyStaticRoute(fileContent.id, app, express)
        for (let collectionItem of fileContent.collection) {
            console.log("  - " + collectionItem.label)
            if (collectionItem.sourcefile) {
                collectionItem.sourcefile = sanitizeSourceFileName(collectionItem.sourcefile)
            }
            const triggerObject: ChatbotCustomTrigger = {
                //triggerType: trigger.startsWith('!') ? TriggerType.command : TriggerType.keyword,
                triggerType: collectionItem.triggers === null ? TriggerType.allmessages : null,
                family: fileContent.id,
                // trigger id is family/sourcefile if sourcefile exists,
                // or family/trigger minus the ! if sourcefile doesn't exist
                id: collectionItem.sourcefile ? `${fileContent.id}/${collectionItem.sourcefile}` : `${fileContent.id}/error`,
                trigger: null,
                label: collectionItem.label,
                outputType: collectionItem.type
            }

            // Add optional attributes
            for (let optionalAttribute of ['parameters', 'sourcefile']) {

                if (optionalAttribute in collectionItem) {
                    triggerObject[optionalAttribute] = collectionItem[optionalAttribute]
                }
            }
            // Load config.json for the command family and add the condig data
            const config = loadTriggerFamilyConfig(fileContent.id)
            if (config) {
                triggerObject.config = config
            }
            if (registeredStaticRoute) {
                // Add the static route if any, so that it is available to overlays
                triggerObject.staticRoute = registeredStaticRoute.route
                // and fix the assets URLs
                fixStaticFilesURLsForCommand(triggerObject, registeredStaticRoute.route)
            }

            // Find and register overlay routes and views
            const overlay = registerOverlayFromChatBotCustomTrigger(triggerObject, app)

            // If there is an overlay, check if the array already contains an overlay with the same route
            // If it does, we don't add it again
            if (overlay) {
                const existingOverlay = overlaysArray.find((existingOverlay) => existingOverlay.route === overlay.route)
                if (existingOverlay) {
                    console.log(`  - Overlay route "${overlay.route}" already exists, skipping`)
                } else {
                    overlaysArray.push(overlay)
                }
            }
            if ( typeof(collectionItem.triggers) !== 'undefined') {
                if (collectionItem.triggers !== null) {
                    for (let trigger of collectionItem.triggers) {
                        const triggerObjectCopy = Object.assign({}, triggerObject)
                        triggerObjectCopy.triggerType = trigger.startsWith('!') ? TriggerType.command : TriggerType.keyword
                        triggerObjectCopy.trigger = trigger
                        triggerObjectCopy.id = collectionItem.sourcefile ? `${fileContent.id}/${collectionItem.sourcefile}` : `${fileContent.id}/${trigger.replace('!', '')}`
                        triggersArray.push(triggerObjectCopy)
                    }
                } else {
                    console.log("  - 'All messages' trigger for " + collectionItem.label + "found");
                    triggerlessArray.push(triggerObject)
                }
            }
            console.log(`  - No triggers for ${collectionItem.label}`)
            if (collectionItem.eventTriggers) {
                for (let eventTrigger of collectionItem.eventTriggers) {
                    const triggerObjectCopy = Object.assign({}, triggerObject)
                    triggerObjectCopy.triggerType = TriggerType.event
                    triggerObjectCopy.trigger = eventTrigger
                    triggerObjectCopy.id = collectionItem.sourcefile ? `${fileContent.id}/${collectionItem.sourcefile}` : `${fileContent.id}/${eventTrigger}`
                    eventTriggersArray.push(triggerObjectCopy)
                }
            }  else {
                console.log(`  - No event triggers for ${collectionItem.label}`)
            }
        }
    }

    return {
        triggers: triggersArray,
        triggerlessArray: triggerlessArray,
        eventTriggers: eventTriggersArray,
        overlays: overlaysArray
    }
}

function loadTriggerFamilyConfig(triggerFamilyId: string): object {
    const configFile = `./commands/${triggerFamilyId}/config.json`
    if (fs.existsSync(configFile)) {
        const textContent = fs.readFileSync(configFile, 'utf8')
        const jsonContent = JSON.parse(textContent)
        jsonContent.family = triggerFamilyId
        return jsonContent
    } else {
        return null
    }
}

function registerOverlayFromChatBotCustomTrigger(command: ChatbotCustomTrigger, app): ChatbotOverlay {
    if (String(command.outputType) == 'overlay') {
        const overlayRoute = registerOverlayRouteFromChatBotCustomTrigger(command, app)
        return {
            "family": command.family,
            "label": command.label,
            "route": overlayRoute
        }
    }
    return null
}

function registerOverlayRouteFromChatBotCustomTrigger(command: ChatbotCustomTrigger, app): string {
    const overlayRoute = `/overlays/${command.family}/${command.sourcefile}/:token`
    console.log(`  - Registering overlay route "${overlayRoute}"`)

    const renderVariables = {
        app_url: global.appURL,
        scheme: config.get('chatbot.scheme'),
        host: config.get('chatbot.host'),
        public_port: config.get('chatbot.public_port'),
        prefix: config.get('chatbot.prefix'),
        command_id: command.id,
        static_route: `${config.get('chatbot.prefix')}${command.staticRoute}`,
        config: command.config,
        htmlPartialPath: `../commands/${command.family}/views/${command.sourcefile}.ejs`
    }

    // Only include css file if it exists on disk
    if (fs.existsSync(`./commands/${command.family}/views/${command.sourcefile}.css`)) {
        renderVariables['cssPartialPath'] = `../commands/${command.family}/views/${command.sourcefile}.css`
    }

    // Only include js file if it exists on disk
    if (fs.existsSync(`./commands/${command.family}/views/${command.sourcefile}.js`)) {
        renderVariables['jsPartialPath'] = `../commands/${command.family}/views/${command.sourcefile}.js`
    }

    app.get(overlayRoute, (req, res) => {
        renderVariables['token'] = req.params.token
        res.render(`overlay`, renderVariables)
    })
    return overlayRoute
}