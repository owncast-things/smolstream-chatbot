interface ChatMessage {
    author?: {
        id: string,
        displayName: string,
    }
    body: string,
    comment?: string
}

// Position of the labels on the alert, on a 3x3 grid
enum LabelPosition {
    TOP_LEFT = 'top-left',
    TOP_CENTER = 'top-center',
    TOP_RIGHT = 'top-right',
    MIDDLE_LEFT = 'middle-left',
    MIDDLE_CENTER = 'middle-center',
    MIDDLE_RIGHT = 'middle-right',
    BOTTOM_LEFT = 'bottom-left',
    BOTTOM_CENTER = 'bottom-center',
    BOTTOM_RIGHT = 'bottom-right',
}

type AlertLabel = {
    id: string,
    text: string,
    position: LabelPosition
}

type SimpleVideoAlert = {
    type: 'simple-video',
    text?: string,
    video: string,
    sound?: string
    duration: number,
}

type SimpleImageAlert = {
    type: 'simple-image',
    text?: string,
    image: string,
    sound?: string
    duration: number,
    labels?: Array<AlertLabel>
}

type SimpleSoundAlert = {
    type: 'simple-sound',
    text?: string,
    sound: string
    duration: number
}

type Alert = SimpleVideoAlert | SimpleImageAlert | SimpleSoundAlert

let queue: Array<Alert> = []
let intervalId: NodeJS.Timeout

function enqueueAlert(alertObject: Alert) {
    queue.push(alertObject);
    // If the queue was empty, start processing it
    if (!intervalId) {
        processAlertsQueue();
    }
}

function dequeueAlert(): Alert {
    const alert = queue.shift();
    return alert
}

function processAlertsQueue() {
    hideAlert();
    let alertObject: Alert = dequeueAlert() ;
    if (alertObject) {
        // Queue is not empty, process the next alert
        displayAlert(alertObject);
        let duration: number = 'duration' in alertObject ? alertObject.duration * 1000 : 5000;
        duration = duration > 15000 ? 15000 : duration;
        
        // Add a 1 second delay between alerts,
        // display alert for its specified duration
        intervalId = setTimeout(function () {
            hideAlert();
            setTimeout(function () { processAlertsQueue(); }, 1000);
        }, duration);
    } else {
        // Queue is empty, stop processing it
        intervalId = null;
    }
}

function hideAlert() {
    const pagePositionGrid = document.getElementById('page-position-grid')
    if (pagePositionGrid) {
        // Stop playing any video or audio before removing the element,
        // not quite sure if it's necessary!
        // If future alert types can have more than one audio or video tag, we'll need to change this
        pagePositionGrid.getElementsByTagName('video').item(0)?.pause()
        pagePositionGrid.getElementsByTagName('audio').item(0)?.pause()
        pagePositionGrid.remove()
    }
}

function displayAlert(alertObject: Alert) {
    switch (alertObject.type) {
        case 'simple-image':
        case 'simple-video':
        case 'simple-sound':
            buildSimpleAlert(alertObject)
            break;
    }
}



function buildSimpleAlert(msg: Alert) {
    // Clone the template grid and set its id
    const pageGrid = document.getElementById('template-page-position-grid').cloneNode(true) as HTMLDivElement;
    pageGrid.id = 'page-position-grid';
    pageGrid.style.removeProperty('display');
    // pageGridMiddleCenter is the middle center slot of the grid: it will receive the image or video,
    // as well as the alertGrid used to set labels in case of a simple-image alert.
    const pageGridMiddleCenter = pageGrid.getElementsByClassName('middle-center')[0] as HTMLElement
    document.body.append(pageGrid);
    switch (msg.type) {
        case 'simple-image':
            pageGridMiddleCenter.style.background = `url(${msg.image}) center no-repeat`;            
            const alertGrid = pageGrid.getElementsByClassName('position-grid')[0] as HTMLDivElement;
            // We need to create an image tag to get the image dimensions so we can size the grid's middle center slot
            const tempImageElement = document.createElement('img')
            tempImageElement.src = msg.image
            // But can get the image dimensions only after the image is loaded...      
            tempImageElement.addEventListener(
                "load",
                () => {
                    const imageHeight = tempImageElement.height
                    const imageWidth = tempImageElement.width
                    // We use the picture's dimensions to size the middle center slot of the grid
                    pageGrid.style.gridTemplateColumns = `1fr ${imageWidth}px 1fr`
                    pageGrid.style.gridTemplateRows = `1fr ${imageHeight}px 1fr`
                    // We also set the size of the alertGrid used to set labels,
                    // so that it is the same size as the image and covers it perfectly
                    alertGrid.style.height = `${imageHeight}px`
                    alertGrid.style.width = `${imageWidth}px`
                    if (msg.labels) {
                        addLabelsToAlert(msg.labels, alertGrid)
                    }
                    AddTextToAlert(msg, pageGrid)
                },
                false
            );            
            break;
        case 'simple-video':
            const videoElement = document.createElement('video')
            videoElement.src = msg.video
            videoElement.autoplay = true
            // We can only get the video's height when it is done loading,
            // so we need to wait for the "playing" event
            videoElement.addEventListener(
                "playing",
                () => {
                    pageGrid.style.gridTemplateColumns = '1fr 100% 1fr'
                    // We use the video's height to size the  middle center slot of the grid
                    // We add 50px for padding :3
                    pageGrid.style.gridTemplateRows = `1fr ${videoElement.videoHeight+50}px 1fr`
                    AddTextToAlert(msg, pageGrid)
                    pageGridMiddleCenter.style.alignSelf = 'center'
                    pageGridMiddleCenter.style.justifySelf = 'center'
                    pageGridMiddleCenter.append(videoElement)
            }, false);
            break;
        case "simple-sound":
            AddTextToAlert(msg, pageGrid)
            break;
    }

    // All alert types can have a sound
    if (msg.sound) {
        const soundElement = document.createElement('audio')
        soundElement.src = msg.sound
        soundElement.autoplay = true
        soundElement.style.display = 'none'
        pageGrid.append(soundElement)
    }
}

// Add the labels on top of the image
// (for now, this is only for simple-image alerts)
function addLabelsToAlert(labels: AlertLabel[], alertPositionGrid: HTMLDivElement) {
    labels.forEach(label => {
        const labelElement = document.createElement('div')
        labelElement.classList.add('alert-label')
        labelElement.innerText = label.text
        alertPositionGrid.getElementsByClassName(String(label.position))[0].append(labelElement)
    })
}

// Set the alert's main text
function AddTextToAlert(alertObject: Alert, pagePositionGrid: HTMLElement) {
    if (alertObject.text) {
        const titleText = pagePositionGrid.getElementsByClassName('title-text')[0] as HTMLElement
        titleText.innerText = alertObject.text
    }
}

