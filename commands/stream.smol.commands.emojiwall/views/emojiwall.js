function getFeatureToggleValue(toggleName, params, defaultValue) {
    const trueValues = ['true', '1'];
    const falseValues = ['false', '0'];
 
    if(trueValues.includes(params.get(toggleName)))
        return true;
    
    if(falseValues.includes(params.get(toggleName)))
        return false;
 
    return defaultValue;
}

function buildClientConfig() {
    let params = new URLSearchParams(document.location.search);
    let host = '';
    if (params.get('host') === null) {
        console.error('Missing host parameter');
    } else {
        host  = atob(params.get('host'));
    }

    let clientConfig = {
        'imagesHost': host,
        'maxCrossingTime': params.get('maxtime'),
        'minCrossingTime': params.get('mintime'),
        'maxEmojiSize': params.get('maxsize'),
        'minEmojiSize': params.get('minsize'),
        'maxEmojis': params.get('maxcount'),
        'effectRotation': {
            'enabled': getFeatureToggleValue('effect-rotation', params, false)
        }
    }

    return clientConfig;
}

window.addEventListener('DOMContentLoaded', (event) => {
    const clientConfig = buildClientConfig();
    initializeEmojiwall(clientConfig);
    // #TODO imagesHost doesn't need to be passed to addEmojis, it's already in the clientConfig
    const image_host = clientConfig.imagesHost;
    socket.on("new_emojis", (msg) => {
        // New emojis received !
        console.log("New emojis received: ", msg)
        addEmojis(JSON.parse(msg), image_host);
    });
});