export default (command, tag) => {
    console.log("overlayexample code!")
    const color = getColor(command.message.body) ?? 'red'
    var regex = new RegExp(`^${color} `, "g");
    const overlayData = {
        "color": color,
        "text": command.message.body.replace(regex, '')
    }
    global.io.to(tag).emit('new_data', JSON.stringify(overlayData))
}

function getColor(messageText: string): string {
    const validColors = ["black", "yellow", "red"]
    // Get first word of message
    const firstWord = messageText.replace(/ .*/,'')
    let color = null
    if (validColors.includes(firstWord)) {
        color = firstWord
    }
    return color
}