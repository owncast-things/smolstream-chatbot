socket.on('new_data', (msg) => {
    // New data received!
    msg = JSON.parse(msg)
    console.log(msg)
    const replaceMe = document.getElementById('replaceme')
    replaceMe.innerText = msg.text
    replaceMe.style.color = msg.color
});