import { exec } from 'child_process';
import { ChatbotCustomTrigger } from '../../interfaces';

const fs = require('fs');

export default async (command: ChatbotCustomTrigger, tag: string) => {
  const query = `sqlite3 -readonly '${command.config['owncast_db_path']}' "SELECT auth.token FROM auth LEFT JOIN user_access_tokens ON auth.user_id = user_access_tokens.user_id LEFT JOIN users ON user_access_tokens.user_id = users.id WHERE users.id = '${command.message.author.id}'"`;

  return new Promise<string>((resolve, reject) => {
    exec(query, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        reject(error);
        return;
      }
      console.log(`stdout: ${stdout}`);
      console.error(`stderr: ${stderr}`);
      resolve(stdout ? `${command.message.author.displayName} is authenticated with "${stdout}"` : `${command.message.author.displayName} is not authenticated.`);
    });
  });
};
