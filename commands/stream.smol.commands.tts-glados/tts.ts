const fs = require("fs");
import * as https from "https";
const querystring = require('querystring');

export default async (command, tag) => {
    // Sanitize the message
    command.message.body = sanitizeMessage(command.message.body)
    const textSlug = command.message.body.replace(/[^a-z0-9]/gi, '_').toLowerCase().substring(0, 20)
    const outputFileName = `${Date.now()}-${textSlug}.wav`
    const ttsData = await tts(command.message.body, `./commands/${command.family}/static/wav/` + outputFileName, command.config)
    global.io.to(tag).emit('new_tts', JSON.stringify(ttsData))
}

// Sanitize the message
function sanitizeMessage(message: string) {
    // Remove all newlines
    message = message.replace(/(\r\n|\n|\r)/gm, "")
    // Remove all HTML tags
    message = message.replace(/(<([^>]+)>)/gi, "")
    // Remove all HTML entities
    message = message.replace(/&[^;]+;/g, "")
    return message
}

async function tts(text: string, outputFilename: string, config: object) {
    // config contains the full url in the form of 'https://host/path'
    const host = config['url'].split('/')[2];
    const path = '/' + config['url'].split('/').slice(3).join('/');
    const postData = querystring.stringify({
        'text': text
    });
    const options = {
        host: host,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postData)
        }
    }
    console.log("glados options", options)
    const data = await ttsSendRequest(options, text, postData, outputFilename)
    return data;
}

/**
 * 
 * @param {Object} options Options for https.request. See https://nodejs.org/api/http.html#httprequestoptions-callback
 * @param {Object} text Request payload.
 * @param {String} outputFilename Filename where to save the data the server sends back.
 */
async function ttsSendRequest(options: https.RequestOptions, text: string, postData: string, outputFilename: string): Promise<{ text: string, wav_filename: string }> {
    const data = await new Promise<Buffer>((resolve, reject) => {
        const request = https.request(options, (response) => {
            const chunks: Buffer[] = [];
            response.on("data", (chunk) => chunks.push(chunk));
            response.on("end", () => resolve(Buffer.concat(chunks)));
        });
        request.on("error", (error) => reject(error));
        request.write(postData);
        request.end();
    });

    await new Promise<void>((resolve, reject) => {
        fs.writeFile(outputFilename, data, (error) => {
            if (error) {
                console.error(`Failed to write file to ${outputFilename}`, error);
                reject(error);
            } else {
                console.log(`File saved to ${outputFilename}`);
                resolve();
            }
        });
    });

    return {
        text,
        wav_filename: outputFilename.split("/").reverse()[0],
    };
}
