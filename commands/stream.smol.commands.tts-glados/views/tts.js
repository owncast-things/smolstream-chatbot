// Fit text to viewport!
// https://dev.to/jankapunkt/make-text-fit-it-s-parent-size-using-javascript-m40#fixed-height-fixed-width

const isOverflown = ({
    clientHeight,
    scrollHeight
}) => scrollHeight > clientHeight

const resizeText = ({
    element,
    elements,
    minSize = 10,
    maxSize = 512,
    step = 1,
    unit = 'px'
}) => {
    (elements || [element]).forEach(el => {
        let i = minSize
        let overflow = false
        const parent = el.parentNode
        while (!overflow && i < maxSize) {
            el.style.fontSize = `${i}${unit}`
            overflow = isOverflown(parent)
            if (!overflow) i += step
        }
        // revert to last state where no overflow happened
        el.style.fontSize = `${i - step}${unit}`
    })
}

var queue = [];
var busy = false;

function enqueueTTS(tts_msg) {
    queue.push(tts_msg);
    // If queue was empty, start processing it
    if (queue.length == 1 && busy == false) {
        processTTSQueue();
    }
}

function dequeueTTS() {
    return queue.shift();
}

var files_list = document.getElementById("files");

socket.on("new_tts", (msg) => {
    console.log('New TTS received from server:', msg)
    enqueueTTS(JSON.parse(msg));
});

function processTTSQueue() {
    busy = true;
    var tts_msg = dequeueTTS();
    // Create audio tag, but play it at the end :
    // it might help with the beginning being cut off.
    var audio_tag = document.createElement('audio');
    var audio_source_tag = document.createElement('source');
    audio_source_tag.setAttribute('src', `<%=prefix%>/static/stream.smol.commands.tts-glados/wav/${tts_msg.wav_filename}`);
    audio_source_tag.setAttribute('type', 'audio/wav');
    audio_tag.appendChild(audio_source_tag);
    var file_item = document.createElement('li');
    file_item.appendChild(audio_tag);

    // When audio is finished playing, remove the onscreen text.
    // and process the next item in the queue.
    audio_tag.addEventListener('ended', function () {
        document.getElementById('now_playing').innerHTML = "";
        if (queue.length > 0) {
            busy = false;
            processTTSQueue();
        }
    }, false);

    // Show text being spoken on screen
    var now_playing_element = document.getElementById('now_playing');
    now_playing_element.innerHTML = tts_msg.text;
    files_list.appendChild(file_item);
    // Fit text to screen
    resizeText({
        elements: document.querySelectorAll('#now_playing'),
        step: 1
    });
    audio_tag.play();

}

