const fs = require("fs");
const https = require("https");

export default async (command, tag) => {
    // Sanitize the message
    command.message.body = sanitizeMessage(command.message.body)
    const voice = command.message.body.split(' ')[0]
    command.message.body = command.message.body.replace(voice, '').trim()
    const textSlug = command.message.body.replace(/[^a-z0-9]/gi, '_').toLowerCase().substring(0, 20)
    const outputFileName = `${Date.now()}-${textSlug}.wav`
    const ttsData = await tts(command.message.body, `./commands/${command.family}/static/wav/` + outputFileName, command.config, voice)
    global.io.to(tag).emit('new_tts', JSON.stringify(ttsData))
}

// Sanitize the message
function sanitizeMessage(message: string) {
    // Remove all newlines
    message = message.replace(/(\r\n|\n|\r)/gm, "")
    // Remove all HTML tags
    message = message.replace(/(<([^>]+)>)/gi, "")
    // Remove all HTML entities
    message = message.replace(/&[^;]+;/g, "")
    return message
}

async function tts(text: string, outputFilename: string, config: object, voice: string) {
    // config only contains the full URL in the form 'https://<host>/instances/<instance id>'
    const host = config['url'].split('/')[2];
    const instanceId = config['url'].split('/')[4];
    const path: string = `/instances/${instanceId}/v1/synthesize?voice=${voice}`;
    const options = {
        host: host,
        path: path,
        method: 'POST',
        auth: 'apikey:' + config['apiKey'],
        headers: {
            "Content-Type": "application/json",
            Accept: "audio/wav",
        },
    }
    const message = {
        text: text,
    };
    const data = await watsonSendRequest(options, JSON.stringify(message), outputFilename)
    return data;
}

/**
 * 
 * @param {Object} options Options for https.request. See https://nodejs.org/api/http.html#httprequestoptions-callback
 * @param {Object} dataToSend Request payload.
 * @param {String} outputFilename Filename where to save the data the server sends back.
 */
function watsonSendRequest(options: Object, dataToSend: string, outputFilename: string) {
    return new Promise((resolve, reject) => {
        // What to do when receiving data back from watson
        function OnResponse(response) {
            // We put the data pieces in this array
            var data = [];
            response.on("data", function (chunk) {
                data.push(chunk);
            });

            response.on("end", function () {
                // When we got all the data, glue all the pieces together
                var buffer = Buffer.concat(data);
                // save the wav file
                fs.writeFile(outputFilename, buffer, function (err) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }
                    console.log(`File saved to ${outputFilename}`);
                });
                const return_object = {
                    text: JSON.parse(dataToSend).text,
                    // Pass only the base filename to the view
                    wav_filename: outputFilename.split('/').reverse()[0]
                }
                resolve(return_object);
            });
        }
        const request = https.request(options, OnResponse);
        console.log('Making watson request with options', options)
        console.log('Making watson request with data', dataToSend)
        request.on('error', error => {
            reject(error);
        });
        // send the request
        request.write(dataToSend);
        request.end();
    });
}