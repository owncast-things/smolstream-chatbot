import express = require("express");
const app = express();
import http = require("http");
import https = require("https");
const server = http.createServer(app);
import { Server } from "socket.io";
global.io = new Server(server);
import bodyParser = require("body-parser");
import glob = require("glob");
import fs = require("fs");
import owncast = require("./src/owncast")
import loading = require("./src/loading")
import outputs = require("./src/outputs")

import {
    ChatbotCustomTrigger,
    ChatMessageEvent,
    TriggerOutputType,
    ImportedJsonData,
    TriggerType,
    ChatEvent,
    EventType
} from "./interfaces"

const config = require("config")

const importedJsonData: ImportedJsonData = loading.loadJsonFiles(app, express)
global.chatbotCommands = importedJsonData.triggers
global.chatbotOverlays = importedJsonData.overlays
global.chatbotTriggerlessCommands = importedJsonData.triggerlessArray
global.chatbotEventCommands = importedJsonData.eventTriggers
console.log("Loaded triggers", global.chatbotCommands)
console.log("Loaded triggerless", global.chatbotTriggerlessCommands)
console.log("Loaded event triggers", global.chatbotEventCommands)

global.appURL = getAppURL()

app.set("view engine", "ejs");
app.use(express.static("out/public"));
app.use(
    bodyParser.urlencoded({
        extended: false,
    })
);
app.use(bodyParser.json());

/**
 * @returns {string} The URL of the app, including the schema, host, port, and prefix.
 * We make the URL as prety as possible, for it is displayed to the user on the homepage.
 **/
function getAppURL(): string {
    const scheme: string = config.get('chatbot.scheme');
    const host: string = config.get('chatbot.host');
    const port: number = config.get('chatbot.public_port');
    const prefix: string = config.get('chatbot.prefix');
    // If host ends with a '/', remove it
    const url_host = host.endsWith('/') ? host.slice(0, -1) : host;
    // If the port is the default for the scheme, don't include it in the URL.
    const url_port = ((port == 80 && scheme == 'http') || (port == 443 && scheme == 'https')) ? '' : `:${port}`;
    // If prefix doesn't start with '/', add it
    const url_prefix = prefix.startsWith('/') ? prefix : `/${prefix}`;

    // Force lowercase on the whole URL just in case
    return `${scheme}://${url_host}${url_port}${url_prefix}`.toLowerCase();
}

// Webhook route
app.post("/hook/chat-message/:token", function (req, res) {
    console.log("Incoming webhook! /hook/chat-message")
    handleChatEvent(owncast.parseOwncastChatEvent(req.body), req.params.token)
    res.json({
        status: 200,
    });
});

// Overlays index
app.get("/overlays/:token", (req, res) => {
    res.render("overlays_index", {
        overlays: global.chatbotOverlays,
        prefix: config.get('chatbot.prefix'),
        token: req.params.token
    })
})

// Alerts overlay
app.get("/alerts/:token", (req, res) => {
    res.render("alerts", {
        app_url: global.appURL,
        scheme: config.get('chatbot.scheme'),
        host: config.get('chatbot.host'),
        public_port: config.get('chatbot.public_port'),
        prefix: config.get('chatbot.prefix'),
        token: req.params.token
    })
})

// Secret chat (streamer control)
app.get("/controlcenter/:token", (req, res) => {
    res.render("control_center", {
        app_url: global.appURL,
        scheme: config.get('chatbot.scheme'),
        host: config.get('chatbot.host'),
        public_port: config.get('chatbot.public_port'),
        prefix: config.get('chatbot.prefix'),
        token: req.params.token
    })
})

function isValidToken(token: string): boolean {
    return true;
}

async function handleChatEvent(event: ChatEvent, token: string) {
    console.log(event)

    if (isValidToken(token)) {
        if (event.type == EventType.chatMessage) {  
            handleChatMessage(event, token)
        } else {

            const foundTriggers = global.chatbotEventCommands.filter((trigger) => {
                return trigger.trigger == event.type
            })
        
            for (let foundTrigger of foundTriggers) {
                switch (foundTrigger.outputType) {
                    case TriggerOutputType.text:
                        postChatMessage(foundTrigger.parameters["text"], token)
                        break;
                    case TriggerOutputType.dynamicText:
                        postChatMessage(await outputs.dynamicText(foundTrigger, token), token)
                        break;
                    case TriggerOutputType.overlay:
                        await outputs.overlay(foundTrigger, token)
                        break;
                    case TriggerOutputType.alert:
                        outputs.alertOverlay(foundTrigger, token)
                        break;
                        default:
                            console.log(`Unknown command type '${foundTrigger.outputType}'.`)
                }
            }
        }
    }
}

async function handleChatMessage(message: ChatMessageEvent, token: string) {

    console.log("Handling chat message", message)
    const foundTrigger: ChatbotCustomTrigger = findTriggerInMessage(message, global.chatbotCommands)
    if (foundTrigger) {
        console.log("Found trigger", foundTrigger)
        switch (foundTrigger.outputType) {
            case TriggerOutputType.text:
                postChatMessage(foundTrigger.parameters["text"], token)
                break;
            case TriggerOutputType.dynamicText:
                postChatMessage(await outputs.dynamicText(foundTrigger, token), token)
                break;
            case TriggerOutputType.overlay:
                await outputs.overlay(foundTrigger, token)
                break;
            case TriggerOutputType.alias:
                console.log(`Alias! Substituting for target command ${foundTrigger.parameters['command']}`)
                // Substitute the alias for the target command
                message.body = `${foundTrigger.parameters["command"]} ${message.body}`
                // Handle the target command
                await handleChatMessage(message, token)
                return
                break;
            case TriggerOutputType.alert:
                outputs.alertOverlay(foundTrigger, token)
                break;
            default:
                console.log(`Unknown command type '${foundTrigger.outputType}'.`)
        }
    }
    // Execute all triggerless commands
    for (let command of global.chatbotTriggerlessCommands) {
        
        // use map to add the message as an attribute too command
        const commandWithMessage = Object.assign({}, command, { message: message });

        console.log('Executing triggerless command', commandWithMessage)
        switch (commandWithMessage.outputType) {
            case TriggerOutputType.text:
                postChatMessage(commandWithMessage.parameters["text"], token)
                break;
            case TriggerOutputType.dynamicText:
                postChatMessage(await outputs.dynamicText(commandWithMessage, token), token)
                break;
            case TriggerOutputType.overlay:
                await outputs.overlay(commandWithMessage, token)
                break;
            case TriggerOutputType.alert:
                outputs.alertOverlay(commandWithMessage, token)
                break;
            default:
                console.log(`Unknown command type '${commandWithMessage.outputType}'.`)
        }
    }

}

// Sanitize the message
function sanitizeMessage(message: string) {
    // Remove all newlines
    message = message.replace(/(\r\n|\n|\r)/gm, "")
    // Remove all HTML tags
    //message = message.replace(/(<([^>]+)>)/gi, "")
    // Remove all HTML entities
    message = message.replace(/&[^;]+;/g, "")
    return message
}

function findTriggerInMessage(message: ChatMessageEvent, triggersArray: Array<ChatbotCustomTrigger>): ChatbotCustomTrigger {
    message.body = sanitizeMessage(message.body)
    // We look for commands first, then for keywords
    // If a message starts with a command, we don't look for anything else
    // i.e. if a message starts with a command we ignore all keywords in the message
    for (let trigger of triggersArray) {
        if (trigger.triggerType === TriggerType.command && (message.body + ' ').startsWith(trigger.trigger + ' ')) {
            trigger.message = message
            trigger.message.body = trigger.message.body.replace(trigger.trigger, '').trim()
            return trigger
        }
    }
    // Now look for keywords
    // TODO handle multiple keywords in the same message?
    for (let trigger of triggersArray) {
        if (trigger.triggerType === TriggerType.keyword && ((' ' + message.body + ' ').includes(' ' + trigger.trigger + ' '))) {
            trigger.message = message
            // trigger id is family/sourcefile if sourcefile exists,
            // or family/trigger if sourcefile doesn't exist
            trigger.id = trigger.sourcefile ? `${trigger.family}/${trigger.sourcefile}` : `${trigger.family}/${trigger.trigger}`
            return trigger
        }
    }
    return null
}

function postChatMessage(message: string, token: string) {
    owncast.postOwncastChatMessage(message, token)
}

server.listen(config.get('chatbot.port'), () => {
    console.log(`listening on *:${config.get('chatbot.port')}`)
    console.log(`public URL: ${global.appURL}`)
})

// client overlays connections
global.io.on("connection", (socket) => {
    console.log("client connected!", socket.handshake.query)
    // Join room based on command name, so that each command's code
    // only send back data to its own client overlay(s)
    const tag = `${socket.handshake.query.command}-${socket.handshake.query.token}`
    console.log("Joining room", tag)
    socket.join(tag)
    socket.on("new_controlcenter-command", (data) => {
        data = JSON.parse(data)
        console.log("new_controlcenter-command", data)
        const chatMessage: ChatMessageEvent = {
            type: EventType.chatMessage,
            author: {
                id: "0",
                displayName: "Control center"
            },
            body: data.message,
            comment: "Sent from control center"
        }
        handleChatMessage(chatMessage, data.token)
    })
})



/* setTimeout(() => {
    handleChatMessage({ body: "!purge2" }, "sometoken")
    handleChatMessage({ body: "!rickroll" }, "sometoken")
    handleChatMessage({ author: { id: '001', displayName: 'Albert'}, body: "!bonk Georgette" }, "sometoken")
    handleChatMessage({ body: "!hydrate" }, "sometoken")
    //handleChatMessage({ body: "!dis Bonjour, je parle Français." }, "sometoken")
    //handleChatMessage({ body: 'Bonjour 👾👾❤️<img class="emoji" alt=":alert:" title=":alert:" src="/img/emoji/alert.gif">' }, "sometoken")
}, 6000) */