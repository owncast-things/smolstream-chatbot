
export interface ChatMessageEvent {
    type: EventType.chatMessage,
    author?: {
        id: string,
        displayName: string,
    }
    body: string,
    comment?: string
}

export enum EventType {
    chatMessage = 'chat-message',
    nameChanged = 'name-changed',
    userJoined = 'user-joined',
    streamStarted = 'stream-started',
    streamStopped = 'stream-stopped',
    visibilityUpdated = 'visibility-updated',
    //streamTitleUpdated = 'stream-title-updated',
}

export interface VisibilityUpdatedEvent {
    type: EventType.visibilityUpdated,
    visible: boolean,
    ids: string[]
}

export interface UserJoinedEvent {
    type: EventType.userJoined,
    userId: string,
    userDisplayName: string,
    isAuthenticated: boolean
}

export interface NameChangeEvent {
    type: EventType.nameChanged,
    userId: string,
    userDisplayName: string,
    previousDisplayName: string,
    isAuthenticated: boolean,
    previousNames: string[],
}

export interface StreamStartedEvent {
    type: EventType.streamStarted,
    streamTitle: string,
    timestamp: number
}

export interface StreamStoppedEvent {
    type: EventType.streamStopped,
    timestamp: number
}


export type ChatEvent = UserJoinedEvent | NameChangeEvent | StreamStartedEvent | StreamStoppedEvent | VisibilityUpdatedEvent | ChatMessageEvent;

export enum TriggerOutputType {
    text = 'text',
    dynamicText = 'dynamictext',
    overlay = 'overlay',
    alias = 'alias',
    alert = 'alert',
}


export enum TriggerType {
    command = 'command',
    keyword = 'keyword',
    allmessages = 'allmessages',
    event = 'event'
}

export interface ChatbotCustomTrigger {
    id?: string,
    triggerType: TriggerType,
    trigger: string
    family: string
    label: string
    sourcefile?: string
    message?: ChatMessageEvent // the chat message, minus the leading '!command ' if any
    parameters?: object
    outputType: TriggerOutputType,
    staticRoute?: string,
    config?: object
}

export interface ChatbotOverlay {
    family: string,
    label: string,
    route: string
}

export interface StaticRoute {
    family: string,
    route: string
}

export interface ImportedJsonData {
    triggers: Array<ChatbotCustomTrigger>,
    triggerlessArray: Array<ChatbotCustomTrigger>,
    eventTriggers: Array<ChatbotCustomTrigger>,
    overlays: Array<ChatbotOverlay> // used for the '/overlays' index page
}


export interface JSONContent {
    id: string;
    label: string;
    collection: {
        triggers?: string[];
        eventTriggers?: string[];
        label: string;
        sourcefile?: string;
        parameters?: object;
        type: TriggerOutputType;
    }[];
}
